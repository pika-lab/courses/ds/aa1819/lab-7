package it.unibo.ds.jade.behaviours;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;

public class DoForever extends DoWhile {

    public DoForever(Behaviour b, Behaviour... bs) {
        super(b, bs);
    }

    public DoForever(Agent agent, Behaviour b, Behaviour... bs) {
        super(agent, b, bs);
    }

    protected boolean checkCondition() {
        return true;
    }


}