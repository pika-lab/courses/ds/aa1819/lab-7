package it.unibo.ds.jade.behaviours;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SerialBehaviour;
import jade.util.leap.Collection;

import java.util.ArrayList;

public abstract class DoWhile extends SerialBehaviour {

    private final ArrayList<Behaviour> subBehaviors;
    private int currentIndex;

    public DoWhile(Behaviour b, Behaviour... bs) {
        this(null, b, bs);
    }

    public DoWhile(Agent agent, Behaviour b, Behaviour... bs) {
        super(agent);
        this.subBehaviors = new ArrayList<>(bs.length + 1);
        this.subBehaviors.add(b);
        for (final Behaviour behaviour : bs) {
            this.subBehaviors.add(behaviour);
        }
    }

    protected abstract boolean checkCondition();

    @Override
    protected void scheduleNext(boolean currentDone, int currentResult) {
        if (currentDone) {
            this.currentIndex = (this.currentIndex + 1) % this.subBehaviors.size();
        }
    }

    @Override
    protected void scheduleFirst() {
        this.currentIndex = 0;
    }

    @Override
    protected boolean checkTermination(boolean currentDone, int currentResult) {
        return currentDone && !checkCondition();
    }

    @Override
    protected Behaviour getCurrent() {
        return this.subBehaviors.get(this.currentIndex);
    }

    @Override
    public Collection getChildren() {
        return new jade.util.leap.ArrayList(this.subBehaviors);
    }
}