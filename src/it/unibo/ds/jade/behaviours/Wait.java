package it.unibo.ds.jade.behaviours;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

import java.time.Duration;

public class Wait extends TickerBehaviour {

    public Wait(Duration period) {
        this(null, period);
    }

    public Wait(Agent a, Duration period) {
        super(a, period.toMillis());
    }

    @Override
    protected void onTick() {
        stop();
        andThen();
    }

    protected void andThen() {

    }
}
