package it.unibo.ds.jade.carsharing;

import java.io.Serializable;
import java.time.Instant;

final class Messages {

    static abstract class CarSharingMessage implements Serializable {
        private final Places leavingPlace;
        private final Places destination;

        CarSharingMessage(Places leavingPlace, Places destination) {
            this.leavingPlace = leavingPlace;
            this.destination = destination;
        }

        Places getLeavingPlace() {
            return leavingPlace;
        }

        Places getDestination() {
            return destination;
        }
    }

    static final class PassageAvailability extends CarSharingMessage implements Serializable {
        private final int nNeededSeats;


        PassageAvailability(Places startingPoint, Places destination, int nNeededSeats) {
            super(startingPoint, destination);
            this.nNeededSeats = nNeededSeats;
        }

        int getNumberOfNeededSeats() {
            return nNeededSeats;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("PassageAvailability{");
            sb.append("leavingPlace=").append(getLeavingPlace());
            sb.append(", destination=").append(getDestination());
            sb.append(", nNeededSeats=").append(nNeededSeats);
            sb.append('}');
            return sb.toString();
        }
    }

    static final class DealProposal extends CarSharingMessage implements Serializable {
        private final int nReservedSeats;
        private final Instant leavingInstant;

        DealProposal(Places startingPoint, Places destination, int nReservedSeats, Instant leavingInstant) {
            super(startingPoint, destination);
            this.nReservedSeats = nReservedSeats;
            this.leavingInstant = leavingInstant;
        }

        int getNumberOfReservedSeats() {
            return nReservedSeats;
        }

        Instant getLeavingInstant() {
            return leavingInstant;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("DealProposal{");
            sb.append("leavingPlace=").append(getLeavingPlace());
            sb.append(", destination=").append(getDestination());
            sb.append(", nReservedSeats=").append(nReservedSeats);
            sb.append(", leavingInstant=").append(leavingInstant);
            sb.append('}');
            return sb.toString();
        }
    }

    static final class PassageProposal extends CarSharingMessage implements Serializable {
        private final int totalSeats;
        private final double costs;
        private final Instant leavingInstant;
        private final int reservedSeats;

        PassageProposal(Places startingPoint, Places destination, int totalSeats, double costs, Instant leavingInstant, int reservedSeats) {
            super(startingPoint, destination);
            this.totalSeats = totalSeats;
            this.costs = costs;
            this.leavingInstant = leavingInstant;
            this.reservedSeats = reservedSeats;
        }

        int getTotalSeats() {
            return totalSeats;
        }

        public double getCosts() {
            return costs;
        }

        public Instant getLeavingInstant() {
            return leavingInstant;
        }

        int getReservedSeats() {
            return reservedSeats;
        }

        int getAvailableSeats() {
            return getTotalSeats() - getReservedSeats();
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("PassageProposal{");
            sb.append("leavingPlace=").append(getLeavingPlace());
            sb.append(", destination=").append(getDestination());
            sb.append(", totalSeats=").append(totalSeats);
            sb.append(", costs=").append(costs);
            sb.append(", leavingInstant=").append(leavingInstant);
            sb.append(", reservedSeats=").append(reservedSeats);
            sb.append('}');
            return sb.toString();
        }
    }
}
