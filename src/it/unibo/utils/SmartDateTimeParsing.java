package it.unibo.utils;

import java.time.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmartDateTimeParsing {
    private static final Pattern dateTimePattern =
            Pattern.compile("^\\s*(?<date>(?<relative>today|yesterday|tomorrow)|(?<exactDate>(?<day>\\d{1,2})/(?<month>\\d{1,2})/(?<year>\\d{4}))\\s)?\\s*(?<hour>\\d{1,2}):(?<minute>\\d{1,2})\\s*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern durationPattern =
            Pattern.compile("^\\s*(\\d+)\\s*(ms|s|m|h|d|ns)$", Pattern.CASE_INSENSITIVE);

    public static Instant parseInstant(final String string) {
        final Matcher m = dateTimePattern.matcher(string);
        if (!m.matches()) {
            throw new IllegalArgumentException("Cannot parse instant: " + string);
        }

        ZonedDateTime date = ZonedDateTime.now();
        final int day, month, year;
        final String relative;

        if (m.group("date") == null | (relative = m.group("relative")) != null) {
            if ("yesterday".equalsIgnoreCase(relative)) {
                date = date.minus(Period.ofDays(1));
            } else if ("tomorrow".equalsIgnoreCase(relative)) {
                date = date.plus(Period.ofDays(1));
            }

            day = date.getDayOfMonth();
            month = date.getMonthValue();
            year = date.getYear();
        } else {
            year = Integer.parseInt(m.group("year"));
            month = Integer.parseInt(m.group("month"));
            day = Integer.parseInt(m.group("day"));
        }

        final ZonedDateTime time = ZonedDateTime.of(
            year,
            month,
            day,
            Integer.parseInt(m.group("hour")),
            Integer.parseInt(m.group("minute")),
            0,
            0,
            ZoneId.systemDefault()
        );

        return Instant.from(time);
    }

    public static Duration parseDuration(final String string) {
        final Matcher match = durationPattern.matcher(string);
        if (!match.matches()) {
            throw new IllegalArgumentException("Cannot parse duration: " + string);
        }
        final long amount = Long.parseLong(match.group(1));
        final String unit = match.group(2);

        if ("ms".equalsIgnoreCase(unit)) {
            return Duration.ofMillis(amount);
        } else if ("s".equalsIgnoreCase(unit)) {
            return Duration.ofSeconds(amount);
        } else if ("m".equalsIgnoreCase(unit)) {
            return Duration.ofMinutes(amount);
        } else if ("h".equalsIgnoreCase(unit)) {
            return Duration.ofHours(amount);
        } else if ("d".equalsIgnoreCase(unit)) {
            return Duration.ofDays(amount);
        } else if ("ns".equalsIgnoreCase(unit)) {
            return Duration.ofNanos(amount);
        } else {
            throw new IllegalStateException("This should never happen!");
        }
    }


}
